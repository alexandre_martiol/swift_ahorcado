//
//  SecondViewController.swift
//  Ahorcado
//
//  Created by Alexandre Martinez Olmos on 08/12/14.
//  Copyright (c) 2014 DAM. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var stringLabel = ""
    var stringImage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        label.text = stringLabel
        image.image = UIImage (named: stringImage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var image: UIImageView!

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let juego: ViewController =
        segue.destinationViewController as ViewController }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
