//
//  ViewController.swift
//  Ahorcado
//
//  Created by Alexandre Martinez Olmos on 27/11/14.
//  Copyright (c) 2014 DAM. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init() {
        super.init()
        var numRandom: Int
    }
    
    @IBOutlet weak var labelWord: UILabel!
    @IBOutlet weak var textFieldChar: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    var wordsToPlay:[String] = ["AHORCADO", "PROGRAMMING", "WIDGET", "ACTIVACION", "CARACULO"]
    var imgToShow = [UIImage (named: "ahorcado1.jpg"), UIImage (named: "ahorcado2.jpg"), UIImage (named: "ahorcado3.jpg")]

    //wordRandom is the word selected of the array
    var wordRandom = String()
    var numRandom = 0
    
    var exist = false
    var win = false
    var numError = 0
    var numSucces = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        numRandom = Int(arc4random_uniform(5))
        wordRandom = wordsToPlay[numRandom]
        
        for item in wordRandom {
            labelWord.text = labelWord.text! + "_ "
        }
        
        exist = false
        win = false
        numError = 0
        numSucces = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func checkChar(sender: AnyObject) {
        //Instance of the sencondViewController
        let secondViewController: SecondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("GameOver") as SecondViewController
        
        var charWrited: Character = Array(textFieldChar.text.uppercaseString)[0]
        //wordPlayed is the word that is showed in the view
        var wordPlayed = labelWord.text

        exist = false
        for (var i = 0; i < countElements(wordRandom); i++) {
            if (Array(wordRandom)[i] == charWrited) {
               wordPlayed = wordPlayed?.stringByReplacingCharactersInRange(Range<String.Index>(start: advance(wordPlayed!.startIndex, i*2), end: advance(wordPlayed!.startIndex, i*2+1)), withString: String(charWrited))
                
                exist = true
                numSucces += 1
            }
        }

        if (Array(wordRandom).count == numSucces) {
            secondViewController.stringLabel = "HAS GANADO"
            secondViewController.stringImage = "winner.jpg"
            
            self.showViewController(secondViewController, sender: self)
        }

        //Si no ha encontrado la letra...
        if (!exist) {
            if (numError < 2) {
                imageView.image = imgToShow[numError]
                
                numError += 1;
            }
            
            else if (numError >= 2) {
                secondViewController.stringLabel = "HAS PERDIDO"
                secondViewController.stringImage = "ahorcado3.jpg"
                
                self.showViewController(secondViewController, sender: self)
            }
        }
        
        labelWord.text = wordPlayed
        textFieldChar.text = ""
        self.view.endEditing(true)
    }
}

